#!/bin/bash
set -u
declare -r SCRIPT_PATH=`realpath $0`;        # chemin  absolu complet du script rep + nom
declare -r SCRIPT_REP=`dirname $SCRIPT_PATH`;# repertoire absolu du script (pas de slash de fin)
declare -r VERSION="v3.0.0-2021.0.6.30";

if [ ! -f "$SCRIPT_REP/legite-lib.sh" ]
then
    echo "$SCRIPT_REP/legite-lib.sh introuvable -> quit"
    exit 1
fi
. $SCRIPT_REP/legite-lib.sh


#############
# VARIABLES #
# DU PROJET #
#############
    declare PSP_sup="";           # parametre supplementaire($@) => $1 $2 $n "$@""
    declare -r CONF_FILE_PRIORITE="$CONF_REP/radioLegral.priorite";

    declare -r PLAYLISTS_ROOT="$SCRIPT_REP/playlists" # chemin relatif au script
    declare -i sortie=0; # demande de sortie du programme

    declare out='stout';

    declare NOFLUX='-';
    declare -A methodTbl;
    declare -A fluxTbl;

    declare method=""  # methode de traitement de PSP_sup
    declare -r FLUX_DEFAUT="$NOFLUX";
    declare flux="$FLUX_DEFAUT";

    declare -i shuffle=0;
    declare -i stop=0;
    declare -i play=0;
    declare -i prev=0;
    declare -i add=0;
    declare -i insert=0;
    declare -i next=0;
    declare -i crop=0;
    declare -i stopVLC=0;

    # - vars calculés - #
    declare -i actionNb=0;     # nb d'action direct demandé (prev,next,stop,etc)

    declare -r PRIORITE_DEFAUT=5;
    declare -i priorite=$PRIORITE_DEFAUT;
    declare -i prioriteLvl=0   # consigne de priorité

    declare cmdProg=''; #command par defaut : rien
    declare cmdOptsMPD=' --host localhost --port 6600';
    declare cmdOptsMPD='';
    declare cmdOptsVLC=' --play-and-exit --no-repeat --recursive expand --no-random --no-loop'
        # Normaliseur de volume
        #      --norm-buff-size <Entier [-2147483648 .. 2147483647]>
        #      --norm-max-level <Flottant>
        #cmdOptsVLC="$cmdOptsVLC --network-caching 3000  --norm-max-level 1"
#


# #################### #
# FONCTIONS GENERIQUES #
# #################### #
function usage(){
    if [ $argNb -eq 0 ]
    then
        echo "$SCRIPT_FILENAME [-d] [--showVars] [--showLibs] [--update] [--conf=fichier.sh] [--out=$out] [--method=$method] librairie|flux";
        echo "--update: update le programme legite-install.sh lui meme (via $UPDATE_HTTP)."
        echo "--conf: execute un fichier de configuration si existe."
        echo "Fichier de configurations:      (visible avec --showVars)."
        echo "programme: execute une fonction (visible avec --showLibs)."
        echo '';
        echo 'programme de sortie:';
        echo ' --out=[stout|cvlc|mpd|defini dans librairie] (default: stout=cvlc)'
        echo ' --stopVLC (killall vlc)'
        echo '';
        echo 'Priorité des flux';
        echo ' Le flux est lu que si priorité >= prioriteLvl (par defaut prioriteLvl=$PRIORITE_DEFAUT)';
        echo ' --priorite= --prioriteLvl=';
        echo '';
        echo 'Commandes de Musique Player Demon';
        echo ' --out=mpd --stop --play --shuffle -aipnc --add --insert --prev --next --crop librairie|flux';
        echo '';
        echo 'Méthodes disponibles: --showVars'

        echo 'collection|librairie|flux:';
        echo 'Charge un fichier collection ou execute une librairie. Si rien de tel alors sera lu comme un flux';
        echo 'Le flux peut etre un fichier,playlist ou un flux réseau.';
        echo 'thinks to add MPD_HOST and MPD_PORT in ~/.bashrc'
        exit $E_ARG_NONE;
    fi
}


function showVarsLocal(){
    fctIn "$*"
    displayVar 'out' "$out";
    displayVar 'PSP_sup ' "$PSP_sup"
    displayVar 'NOFLUX  ' "$NOFLUX" 'FLUX_DEFAUT' "$FLUX_DEFAUT" 'flux' "$flux";
    displayVar 'shuffle ' "$shuffle" 'stop' "$stop" 'play' "$play" 'prev' "$prev" 'add' "$add" 'insert' "$insert" 'next' "$next" 'crop' "$crop"
    displayVar 'stopVLC ' "$stopVLC"
    displayVar 'actionNb' "$actionNb";
    displayVar 'PRIORITE_DEFAUT' "$PRIORITE_DEFAUT" 'priorite' "$priorite" 'prioriteLvl' "$prioriteLvl";
    h2 "Methodes:"
    displayVar 'method' "$method" 'methodTbl' "${methodTbl[*]}"
    h2 "Commandes:"
    displayVar 'cmdProg' "$cmdProg" 'cmdOptsMPD' "$cmdOptsMPD" 'cmdOptsVLC' "$cmdOptsVLC" 'stopVLC' "$stopVLC"
    h2 "Commandes MPC:"
    displayVar 'add' "$add" 'insert' "$insert" 'prev' "$prev" 'next' "$next" 'crop' "$crop" 'shuffle' "$shuffle"
    fctOut "$FUNCNAME";return 0;
}



################
# MISE A JOURS #
################
# - telecharge et installe la derniere version ainsi que les fichiers de configurations par defauts - #
function selfUpdateLocal(){
    if [ $isUpdate -eq 1 ]
    then
        if [ $IS_ROOT -eq 1 ]
        then
            echo "version actuelle: $VERSION"
            if [ ! -d "$CONF_ETC_ROOT"  ];then mkdir "$CONF_ETC_ROOT";fi
            if [ ! -d "$CONF_USER_ROOT" ];then mkdir "$CONF_USER_ROOT";fi
            echo "mise a jours du programme";
            updateProg "$UPDATE_HTTP/$SCRIPT_FILENAME"   "/usr/local/bin/$SCRIPT_FILENAME"
            updateProg "$UPDATE_HTTP/$CONF_ETC_FILENAME" "$CONF_ETC_PATH"
            #updateProg "$UPDATE_HTTP/$CONF_ORI_FILENAME" "$CONF_ORI_PATH"
            #updateProg "$UPDATE_HTTP/CONF_USER_FILENAME" "$CONF_USER_PATH"
            updateProg "$UPDATE_HTTP/gammes.sh" "$CONF_ETC_ROOT/gammes.sh"

            # affiche la nouvelle version
            local newVersion=$($SCRIPT_PATH -V);
            echo "Mise à jours de la version $VERSION vers la version: $newVersion."
        else
            echo "Vous devez etre root pour mettre a jours le programme."
        fi
    fi
}


# ################### #
# functions du projet #
# ################### #
function h1(){
    local_txt={1:""}
    echo $GREEN$_txt$NORMAL
}

function h2(){
    local_txt={1:""}
    echo $MAGENTA$_txt$NORMAL
}


# - fonction executer a chaque sortie du programme - #
# $1: code retour a transmettre
function sortie(){
    exit $1
}


# - gestion de la consigne de priorite - #
function setPrioriteLvl (){
    fctIn "$*"

    if [ $# -eq 0 ]
    then
        prioriteLvl=$PRIORITE_DEFAUT;
    else
        if [ $1 -ge 0 ]
        then
            prioriteLvl=$1;
            echo $prioriteLvl > $CONF_FILE_PRIORITE
        fi
    fi
    fctOut "$FUNCNAME";return 0;

}


function loadPrioriteLvl (){
    fctIn "$*"
    # charge depuis le fichier
    displayDebug "Chargement de la priorité via $CONF_FILE_PRIORITE";

    #if [ -s "$CONF_FILE_PRIORITE" ] 
    if [ -r "$CONF_FILE_PRIORITE" ] ;
    then
        displayDebug "Le fichier $CONF_FILE_PRIORITE est lisible.";

        if [ ! -f  $CONF_FILE_PRIORITE ];
        then
            echo "$PRIORITE_DEFAUT" > $CONF_FILE_PRIORITE;
        fi

        prioriteLvl=$(cat $CONF_FILE_PRIORITE)
        displayDebug "prioriteLvl=$prioriteLvl";

        # verifie la valeur chargee
        if [ $prioriteLvl -lt 0 ]
        then
            prioriteLvl=$PRIORITE_DEFAUT;
        fi
    else # pas de fichier lisible
        prioriteLvl=$PRIORITE_DEFAUT
        echo "$prioriteLvl" > $CONF_FILE_PRIORITE
    fi
    fctOut "$FUNCNAME";return 0;
}


# execute la commande si priorite est au moins egal a prioriteLvl
function execCmd(){
    fctIn "$*"
    #if [ ! "$cmdProg" = "" ]
    #then
        if [ $priorite -ge $prioriteLvl ]
        then
            evalCmd "$1";
        else
            echo "La priorité du flux ($priorite) est inferieur à la consigne ($prioriteLvl)";
        fi
    #fi
    fctOut "$FUNCNAME";return 0;
}



# ################################ #
# METHODE DE TRAITEMENT DE PSP_sup #
# ################################ #
    function isMethodExist(){
        if [ $# -eq 0 ]; then return $E_ARG_REQUIRE;fi
        local _method=${1:-""}
        if [ "$methodNom" == '' ];then return $E_ARG_EMPTY;fi
        for value in "${methodTbl[@]}";
        do
            if [ "$value" == "$methodNom" ]; then return $E_FALSE; fi
        done;
        fctOut "$FUNCNAME";return $E_FALSE;

    }

    function execMethod(){
        fctIn "$*"
        if [ $isTrapCAsk -eq 1 ];then break;fi

        local methodNom=${1:-''}
        if [ "$methodNom" == '' ]
        then
            displayDebug 'Méthode vide';
            return $E_ARG_REQUIRE;
        fi
        local methodCall=${methodTbl[$methodNom]:-''}
        if [ "$methodCall" == '' ]
        then
            echo $WARN"Méthode '$methodCall' inexistante!"$NORMAL
        fi

        methodCall="method_$methodCall"

        displayVar "methodCall/methodNom" "$methodCall/$methodNom";

        isMethodExist "$methodNom"
        local -i errNu=$?
        displayDebugdVar "$LINENO:isMethodExist" "$errNu"
        #case $errNu in
        #68)  #$E_ARG_EMPTY
        #    echo $WARN"Methode vide"$NORMAL
        #    fctOut "$FUNCNAME";return $E_FALSE;
        #    ;;

        #1)
        #    displayDebug "Lancement de $methodCall";
        #    evalCmd "$methodCall";
        #    displayDebugdVar "$LINENO:flux" "$flux"
        #    fctOut "$FUNCNAME";return $E_FALSE;
        #    ;;

        #2)
        #    displayDebug "Méthode non défini";
        #    fctOut "$FUNCNAME";return $E_FALSE;
        #    ;;

        #esac
        fctOut "$FUNCNAME";return 0;
    }
#


# ############ #
# LES METHODES #
# ############ #
    function method_firstInFile(){
        fctIn "$*"
        if [ ! -f "$PSP_sup" ]
        then
            echo $WARN"Methode '$FUNCNAME': Fichier $PSP_sup introuvable."$NORMAL
            return $E_FALSE;
        fi
        
        # recuperation de la derniere ligne
        flux=$(head --lines=1 $PSP_sup)
        if [ ! -f "$flux" ]
        then
            echo $WARN"Methode '$FUNCNAME': Le flux '$flux' fournis par '$PSP_sup' est introuvable."$NORMAL
            return E_FALSE;
        fi
    }
    methodTbl['firstInFile']=firstInFile

    function method_firstInFileAndCut(){
        fctIn "$*"
        method_firstInFile

        # Suppression de la 1ere ligne
        sed  -i '1d' $PSP_sup
        if [ $? -ne 0 ]
        then
            echo $WARN"Methode '$FUNCNAME': Problème lors de la suppression de la premiere ligne de $PSP_sup."$NORMAL
        fi 
    }
    methodTbl['firstInFileAndCut']=firstInFileAndCut

    function method_lastInFile(){
        fctIn "$*"
        if [ ! -f "$PSP_sup" ]
        then
            echo $WARN"Methode '$FUNCNAME': Fichier $PSP_sup introuvable."$NORMAL
            return 1;
        fi
        
        # recuperation de la derniere ligne
        flux=$(tail --lines=1 $PSP_sup)
        if [ ! -f "$flux" ]
        then
            echo $WARN"Methode '$FUNCNAME': Le flux '$flux' fournis par '$PSP_sup' est introuvable."$NORMAL
            return 2;
        fi
    }
    methodTbl['lastInFile']=lastInFile

    function method_lastInFileAndCut(){
        fctIn "$*"
        method_lastInFile

        # Suppression de la derniere ligne
        sed  -i '$d' $PSP_sup
        if [ $? -ne 0 ]
        then
            echo $WARN"Methode '$FUNCNAME': Problème lors de la suppression de la derniere ligne de $PSP_sup."$NORMAL
        fi 
    }
    methodTbl['lastInFileAndCut']=lastInFileAndCut
#

# ########## #
# PARAMETRES #
# ########## #
#isDebug=1; # for debuf le PSP
#TEMP=$(getopt \
#    --options vdVhnS- \
#    --long help,version,verbose,debug,showVars,showCollections,update,conf::,showLibs,showLibsDetail,showDuree\
#    --options v::dVhnS-aipnc \
#    --long help,version,verbose::,debug,showVars,showCollections,showLibs,update,conf::\
TEMP=$(getopt \
    --options vdVhnS-aipnc \
    --long help,version,verbose,debug,update,showVars,showCollections,conf::,showLibs,showLibsDetail,showDuree\
,method::,out::,prioriteLvl::,priorite::,play,stop,shuffle,add,insert,prev,next,crop \
        -- "$@")
    eval set -- "$TEMP"

    while true
    do
        if [ $isTrapCAsk -eq 1 ];then break;fi
        argument=${1:-""}
        parametre=${2:-""}
        #displayVarDebug 'argument' "$argument" 'parametre' "$parametre"

        case "$argument" in

            # - fonctions generiques - #
            -h|--help)    usage; exit 0;                 ;;
            -V|--version) echo "$VERSION"; exit 0;       ;;
            -v|--verbose) ((verbosity++));               ;;
            -d|--debug)   ((isDebug++));isShowVars=1;isShowDuree=1; ;;
            --update)           isUpdate=1;              ;;
            --showVars)         isShowVars=1;            ;;
            --showCollections)  isShowCollections=1;     ;;
            --showLibs )        ((isShowLibs++));        ;;
            --showLibsDetail)   isShowLibs=2;            ;;
            --showDuree )       isShowDuree=1;           ;;
            --conf)     CONF_PSP_PATH="$parametre";shift ;;

            -S)                 isSimulation=1;          ;;

            # - parametres liés au projet - #

            # - methode de traitement  - #
            --method)   method="$2";   shift  ;;

            # - programme utilisé pour lire le flux - #
            --out)       out="$2";     shift  ;;

            # changer le niveau globale de la priorite
            --prioriteLvl) setPrioriteLvl $2; shift   ;;

            # definir le priorite du flux
            --priorite)       priorite=$2;     ;;

            --play)      out="mpd"; play=1;   ;;
            --stop)      out="mpd"; stop=1;    ;;
            --shuffle)   out="mpd"; shuffle=1;shit ;;
            -a|--add)    out="mpd"; add=1;     ;;
            -i|--insert) out="mpd"; insert=1;  ;;
            -p|--prev)   out="mpd"; prev=1;    ;;
            -n|--next)   out="mpd"; next=1;    ;;
            -c|--crop)   out="mpd"; crop=1;    ;;
        
            # - Les parametres supplementaires - #
            --) # créer pat getopt; juste ignoré ce parametre
                ;;

            -)  # indicateur des parametres pour les librairies/collections
                ((isPSPSup++));
                ;;

            #--) # parcours des arguments supplementaires


            *) #default $argument,$parametre"
                if [[ -z "$argument" ]];then break;fi
                if [[ $isPSPSup -eq 0 ]]
                then
                    librairie="$argument";
                    isLibExist "$librairie"
                    if [[ $? -eq $E_TRUE ]]
                    then
                        displayDebug "'$argument': est une librairie"
                        librairiesCall[$librairiesCallIndex]="$librairie"
                        ((librairiesCallIndex++))
                    else # c'est un flux
                        displayDebug "'$argument' est un flux"
                        flux="$argument"
                        #if [[ ! -r "$flux" ]]
                        #then
                        #    erreursAddEcho "Le flux '$flux' n'est pas lisible"
                        #    flux="$NOFLUX"
                        #fi
                    fi
                elif [[ $isPSPSup -eq 1 ]] # est ce un parametre de librairie?
                then
                    addLibParams "$argument";
                fi
                ;;
        esac

        shift 1; #renvoie une valeur non null
        if [[ -z "$parametre" ]];then break;fi
    done
#


########
# main #
########
beginStandard
loadPrioriteLvl



#################################
# EXECUTION DES ACTIONS DIRECTS #
#################################
if [ $stopVLC -eq 1 ]
then
    evalCmd "killall vlc";
fi


############################
# TRAITEMENT DE LA PSP_sup #
############################
if [ "$PSP_sup" != '' ]
then
    execMethod "$method"
    if [ "$flux" == "$NOFLUX" ] #si la methode n'a pas definit de flux
    then
        # est ce une librairie?
        isLibExist "$PSP_sup"
        if [ $? -eq 1 ]
        then
            displayDebug "execution de la librairie: $PSP_sup"
            evalCmd "$PSP_sup";
        else
            displayDebug "Ce n'est pas une librairie: flux direct($PSP_sup)"
            flux="$PSP_sup"
        fi
    fi
    displayVarDebug "$LINENO:flux" "$flux"
fi


#############################
# GESTION DES SORTIES (out) #
#############################
###################
# -- out:stout -- #
###################
    if [ "$out" = "stout" ]
    then
        displayDebug "out:$out"
        out="cvlc";
    fi
#

###################
# -- out: cvlc -- #
###################
    if [ "$out" = "cvlc" ]
    then
        displayDebug "out:$out"
        cmdProg="$out";
    fi
#

###################
# -- out:  mpd -- #
###################
    if [ "$out" = "mpd" ]
    then
        #echo "out= mpd"
        #cmdProg="mpc";

    #    if [ $actionNb -eq 0 ]; then
    #        if [ "$flux" != "" ];then
    #            add=1; # add: action par defaut si 1 flux preciser 
    #        fi
    #    fi # if [ $actionNb ];
    
        # action immediate (sans flux)
        if [ $play -eq 1 ];   then execCmd "mpc $cmdOptsMPD play 1"; fi
        if [ $stop -eq 1 ];   then execCmd "mpc $cmdOptsMPD stop"; fi
        if [ $shuffle -eq 1 ];then execCmd "mpc $cmdOptsMPD shuffle"; fi
        if [ $prev -eq 1 ];   then execCmd "mpc $cmdOptsMPD prev"; fi
        if [ $next -eq 1 ];   then execCmd "mpc $cmdOptsMPD next"; fi
        if [ $crop -eq 1 ];   then execCmd "mpc $cmdOptsMPD crop"; fi
        # action immediate (avec flux)
        if [ $add -eq 1 ];    then execCmd "mpc $cmdOptsMPD add '$flux'"; fi
        if [ $insert -eq 1 ]; then execCmd "mpc $cmdOptsMPD insert '$flux'"; fi

        #sortie 1
        #cmdProg=""; #desactivation de la commande par defaut
    fi #mpd
#


###############################
# - ROUTAGE SELON $cmdProg - #
###############################
    case "$cmdProg" in

        cvlc )
            if [ "$flux" = "$NOFLUX" ]
            then
                echo "pas de flux fournis.";
                cmdProg=""; #void
            else
                displayVar 'flux:0' "${flux:0:1}"
                if [  "${flux:0:1}" = '~' ]
                then
                    cmdProg="$cmdProg $cmdOptsVLC $flux 2>/dev/null";
                else
                    cmdProg="$cmdProg $cmdOptsVLC '$flux' 2>/dev/null";
                fi
                #echo  "$flux" > $pcLastFile	# last playing playlist
            fi
            ;;
            
        *) # la librairie defini son programme a executer avec ses parametres via $cmdProg
            displayDebug "$cmdProg";
            ;;
    esac
#

#############################
# - EXECUTION de $cmdProg - #
#############################
execCmd "$cmdProg";

#showVarsPost
endStandard

if [ $isTrapCAsk -eq 1 ];then exit $E_CTRL_C;fi
exit 0